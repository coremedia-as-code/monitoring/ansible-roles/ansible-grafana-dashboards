import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/var/lib/grafana/dashboards/backend",
    "/var/lib/grafana/dashboards/frontend",
    "/var/lib/grafana/dashboards/delivery",
    "/var/lib/grafana/dashboards/licenses",
    "/var/lib/grafana/dashboards/databases",
    "/var/lib/grafana/dashboards/overviews",
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists
    assert d.group == "grafana"
    assert d.mode == 0o755


@pytest.mark.parametrize("files", [
    "/etc/grafana/provisioning/dashboards/backend.yml",
    "/etc/grafana/provisioning/dashboards/frontend.yml",
    "/etc/grafana/provisioning/dashboards/delivery.yml",
    "/etc/grafana/provisioning/dashboards/licenses.yml",
    "/etc/grafana/provisioning/dashboards/databases.yml",
    "/etc/grafana/provisioning/dashboards/overviews.yml",
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file
    assert f.group == "grafana"
    assert f.mode == 0o640
